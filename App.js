import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import UsersList from './src/usersList';
import UserDetails from './src/usersDetails';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='ListUsers'>
        <Stack.Screen name='ListaUsuarios' component={UsersList} options={{title:'Lista de Usuários'}} />
        <Stack.Screen name='DetalhesUsuario' component={UserDetails} options={{title:'Informações do Usuário'}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
