import React, {useEffect, useState} from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";
import axios from "axios";

const UsersList = ({navigation})=>{

    const [users, setUsers] = useState([]);

    useEffect(()=>{
        getUsers();
    },[])

    async function getUsers(){
        try{
            const response = await axios.get('https://jsonplaceholder.typicode.com/users');
            setUsers(response.data);

        }catch(error){
            console.error(error)
        }
    }




    const userPress=(user)=>{
        navigation.navigate('DetalhesUsuario', {user});
    };

    return(
        <View>
            <FlatList 
            data={users}
            keyExtractor={(item)=> item.id.toString}
            renderItem={({item})=>(
                <TouchableOpacity style={styles.container} onPress={()=> userPress(item)}>
                    <Text style={styles.texto}>{item.name}</Text>
                </TouchableOpacity>
            )}/>
        </View>
    );

}

const styles = StyleSheet.create({
    texto: {
        fontSize: 26,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0.2,
        backgroundColor: 'lightgrey',
        marginBottom: 18,
        paddingTop: 5,
        paddingBottom: 5,
    }

});


export default UsersList;