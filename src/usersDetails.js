import React from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";

const UserDetails = ({route})=>{
    const {user} = route.params;
    return(
        <View style={styles.container}>
            <Text style={styles.texto}>Nome: <Text style={styles.info}>{user.name}</Text></Text>
            <Text style={styles.texto}>Username: <Text style={styles.info}>{user.username}</Text></Text>
            <Text style={styles.texto}>Email: <Text style={styles.info}>{user.email}</Text></Text>
            <Text style={[styles.texto, {borderBottomWidth: 0}]}>Endereço: </Text>
            <Text style={[styles.texto, styles.lista]}>Rua: <Text style={styles.info}>{user.address.street}</Text></Text>
            <Text style={[styles.texto, styles.lista]}>Cidade: <Text style={styles.info}>{user.address.city}</Text></Text>
            <Text style={[styles.texto, styles.lista]}>Código Postal: <Text style={styles.info}>{user.address.zipcode}</Text></Text>
            <Text style={styles.texto}>Telefone: <Text style={styles.info}>{user.phone}</Text></Text>
            <Text style={styles.texto}>Website: <Text style={styles.info}>{user.website}</Text></Text>
            <Text style={styles.texto}>Empresa: <Text style={styles.info}>{user.company.name}</Text></Text>
            <Text style={styles.texto}>Frase de efeito: <Text style={styles.info}>{user.company.catchPhrase}</Text></Text>
        </View>
    )
}

const styles = StyleSheet.create({
    texto: {
        fontSize: 20,
        fontWeight: 'bold',
        borderBottomWidth: 0.2,
        width: '100%',
        paddingLeft: 6,
        borderColor: 'rgba(0, 0, 0, 0.5)'
    },
    lista:{
        marginLeft: 14
    },
    info: {
        fontSize: 20,
        fontWeight: 'normal',
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        gap: 20,
    }

});

export default UserDetails;
